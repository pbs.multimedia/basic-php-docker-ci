FROM bitnami/nginx:1.16

WORKDIR /app

COPY /src .

COPY config/nginx.conf /opt/bitnami/nginx/conf/server_blocks/nginx.conf

